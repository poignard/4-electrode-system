# 4-electrode-system

This code enables to compute the static electric field in the 4-electrode-system for liver electroporation. The code generates the mesh with Gmsh, and compute the solution of the nonlinear problem with Freefem++.


Program to run the simulations of the static nonlinear model of tissue electroporation in the 4-electrodes setup describes in the paper.

**Program requirements**

The following programs must be installed to run the code:
* 		freefem++ (https://freefem.org/, a free finite element solver)
* 		Gmsh 4.4.1        (http://gmsh.info/, a 3D mesh generator)

**Files description**

The repository is composed by 8 files
* 		system_3D.geo, system_2D_xy.geo and system_2D_xz.geo:
 		These Gmsh files generate the meshes needed
* 		create_meshes.edp: 
        This code is integrated to the MainCode.edp file to generate the meshes from .geo files. 
        Beware to enter the full path where the executable gmsh is put in this file.
* 		params.txt : 
        This text file contains the parameters of the model.
* 		read_file.edp: 
        this Freefem++ file reads the parameters set in params.txt, and 
        attributes the corresponding values to the variables in the MainCode.edp file.
* 		MainCode.edp: 
        the Freefem++ main file, which computes the solution of the nonlinear static model of electroporation. 
        The numerical scheme used is in Supplementary II of the paper.

**To run the code**

Run the code in a terminal (from within the code folder) as follows:

`Freefem++ -v 0 MainCode.edp`

The code generates the meshes in the Meshes folder, and the simulations of the electric field are set in the Results folder in the .vtk format (open them with paraview for instance).
In the terminal the number of iterations, the L2 norm gradient of last iteration minus the previous one and the maximal norm of the last iteration minus the previous one are given to estimate the accuracy of the computation, and see if the steady state has been reached.
The nominal electric field (applied voltage divided by the distance of the electrodes) and the computed resistance of the tissue are given.

**Parameters**

The parameters are by default those used in the paper. You can adjust the parameters in the  params.txt file to modify the applied voltage magnitude, the tissue conductivities, the number of iterations…
How to refine/coarsen the mesh
Adjust the parameters lc in the .geo files to refine or coarsen the mesh. Beware that the mesh unit is in mm.
