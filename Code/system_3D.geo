
SetFactory("OpenCASCADE");

Mesh.MshFileVersion = 2.2;

lc = 0.0001*1000;

r = 0.000125*1000;
h = 0.003*1000;
l = 0.0028*1000;
R = 0.000225*1000;
H = 0.0063*1000;
L = 0.0046*1000;

A = 0.008*1000;
a = 0.004*1000;
B = 0.010*1000;

Abis = 0.005*1000;
abis = 0.001*1000;
Bbis = 0.007*1000;

// electrode -v

Point(1) = {-l/2,0,0,lc};
Point(2) = {-l/2+r,0,0,lc};
Point(3) = {-l/2,r,0,lc};
Point(4) = {-l/2-r,0,0,lc};
Point(5) = {-l/2,-r,0,lc};

Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};

Point(6) = {-l/2,0,-h,lc};
Point(7) = {-l/2+r,0,-h,lc};
Point(8) = {-l/2,r,-h,lc};
Point(9) = {-l/2-r,0,-h,lc};
Point(10) = {-l/2,-r,-h,lc};

Circle(5) = {7,6,8};
Circle(6) = {8,6,9};
Circle(7) = {9,6,10};
Circle(8) = {10,6,7};


Line(9) = {2,7};
Line(10) = {3,8};
Line(11) = {4,9};
Line(12) = {5,10};


Point(11) = {-l/2,0,-h-r,lc};

Circle(13) = {8,6,11};
Circle(14) = {11,6,10};
Circle(15) = {9,6,11};
Circle(16) = {11,6,7};


// electrode +v

Point(21) = {+l/2,0,0,lc};
Point(22) = {+l/2+r,0,0,lc};
Point(23) = {+l/2,r,0,lc};
Point(24) = {+l/2-r,0,0,lc};
Point(25) = {+l/2,-r,0,lc};

Circle(21) = {22,21,23};
Circle(22) = {23,21,24};
Circle(23) = {24,21,25};
Circle(24) = {25,21,22};

Point(26) = {+l/2,0,-h,lc};
Point(27) = {+l/2+r,0,-h,lc};
Point(28) = {+l/2,r,-h,lc};
Point(29) = {+l/2-r,0,-h,lc};
Point(30) = {+l/2,-r,-h,lc};

Circle(25) = {27,26,28};
Circle(26) = {28,26,29};
Circle(27) = {29,26,30};
Circle(28) = {30,26,27};


Line(29) = {22,27};
Line(30) = {23,28};
Line(31) = {24,29};
Line(32) = {25,30};


Point(31) = {+l/2,0,-h-r,lc};

Circle(33) = {28,26,31};
Circle(34) = {31,26,30};
Circle(35) = {29,26,31};
Circle(36) = {31,26,27};


// electrode +i

Point(41) = {+L/2,0,0,lc};
Point(42) = {+L/2+R,0,0,lc};
Point(43) = {+L/2,R,0,lc};
Point(44) = {+L/2-R,0,0,lc};
Point(45) = {+L/2,-R,0,lc};

Circle(41) = {42,41,43};
Circle(42) = {43,41,44};
Circle(43) = {44,41,45};
Circle(44) = {45,41,42};

Point(46) = {+L/2,0,-H,lc};
Point(47) = {+L/2+R,0,-H,lc};
Point(48) = {+L/2,R,-H,lc};
Point(49) = {+L/2-R,0,-H,lc};
Point(50) = {+L/2,-R,-H,lc};

Circle(45) = {47,46,48};
Circle(46) = {48,46,49};
Circle(47) = {49,46,50};
Circle(48) = {50,46,47};


Line(49) = {42,47};
Line(50) = {43,48};
Line(51) = {44,49};
Line(52) = {45,50};


Point(51) = {+L/2,0,-H-R,lc};

Circle(53) = {48,46,51};
Circle(54) = {51,46,50};
Circle(55) = {49,46,51};
Circle(56) = {51,46,47};




// electrode -i

Point(61) = {-L/2,0,0,lc};
Point(62) = {-L/2+R,0,0,lc};
Point(63) = {-L/2,R,0,lc};
Point(64) = {-L/2-R,0,0,lc};
Point(65) = {-L/2,-R,0,lc};

Circle(61) = {62,61,63};
Circle(62) = {63,61,64};
Circle(63) = {64,61,65};
Circle(64) = {65,61,62};

Point(66) = {-L/2,0,-H,lc};
Point(67) = {-L/2+R,0,-H,lc};
Point(68) = {-L/2,R,-H,lc};
Point(69) = {-L/2-R,0,-H,lc};
Point(70) = {-L/2,-R,-H,lc};

Circle(65) = {67,66,68};
Circle(66) = {68,66,69};
Circle(67) = {69,66,70};
Circle(68) = {70,66,67};


Line(69) = {62,67};
Line(70) = {63,68};
Line(71) = {64,69};
Line(72) = {65,70};


Point(71) = {-L/2,0,-H-R,lc};

Circle(73) = {68,66,71};
Circle(74) = {71,66,70};
Circle(75) = {69,66,71};
Circle(76) = {71,66,67};


// box

Point(81) = {A,a,0,8*lc};
Point(82) = {-A,a,0,8*lc};
Point(83) = {-A,-a,0,8*lc};
Point(84) = {A,-a,0,8*lc};

Line(81) = {81,82};
Line(82) = {82,83};
Line(83) = {83,84};
Line(84) = {84,81};

Point(85) = {A,a,-B,8*lc};
Point(86) = {-A,a,-B,8*lc};
Point(87) = {-A,-a,-B,8*lc};
Point(88) = {A,-a,-B,8*lc};

Line(85) = {85,86};
Line(86) = {86,87};
Line(87) = {87,88};
Line(88) = {88,85};


Line(89) = {81,85};
Line(90) = {82,86};
Line(91) = {83,87};
Line(92) = {84,88};


// small box

Point(101) = {Abis,abis,0,2*lc};
Point(102) = {-Abis,abis,0,2*lc};
Point(103) = {-Abis,-abis,0,2*lc};
Point(104) = {Abis,-abis,0,2*lc};

Line(101) = {101,102};
Line(102) = {102,103};
Line(103) = {103,104};
Line(104) = {104,101};

Point(105) = {Abis,abis,-Bbis,2*lc};
Point(106) = {-Abis,abis,-Bbis,2*lc};
Point(107) = {-Abis,-abis,-Bbis,2*lc};
Point(108) = {Abis,-abis,-Bbis,2*lc};

Line(105) = {105,106};
Line(106) = {106,107};
Line(107) = {107,108};
Line(108) = {108,105};


Line(109) = {101,105};
Line(110) = {102,106};
Line(111) = {103,107};
Line(112) = {104,108};


//+
Curve Loop(11) = {71, -66, -70, 62};
//+
Surface(7) = {11};
//+
Curve Loop(13) = {70, -65, -69, 61};
//+
Surface(8) = {13};
//+
Curve Loop(15) = {69, -68, -72, 64};
//+
Surface(9) = {15};
//+
Curve Loop(17) = {67, -72, -63, 71};
//+
Surface(10) = {17};
//+
Curve Loop(19) = {75, -73, 66};
//+
Surface(11) = {19};
//+
Curve Loop(21) = {73, 76, 65};
//+
Surface(12) = {21};
//+
Curve Loop(23) = {68, -76, 74};
//+
Surface(13) = {23};
//+
Curve Loop(25) = {67, -74, -75};
//+
Surface(14) = {25};
//+





//+
Curve Loop(28) = {12, 8, -9, -4};
//+
Surface(16) = {28};
//+
Curve Loop(30) = {7, -12, -3, 11};
//+
Surface(17) = {30};
//+
Curve Loop(32) = {2, 11, -6, -10};
//+
Surface(18) = {32};
//+
Curve Loop(34) = {10, -5, -9, 1};
//+
Surface(19) = {34};
//+
Curve Loop(36) = {15, -13, 6};
//+
Surface(20) = {36};
//+
Curve Loop(38) = {13, 16, 5};
//+
Surface(21) = {38};
//+
Curve Loop(40) = {8, -16, 14};
//+
Surface(22) = {40};
//+
Curve Loop(42) = {14, -7, 15};
//+
Surface(23) = {42};
//+
Curve Loop(44) = {2, 3, 4, 1};
//+
Plane Surface(24) = {44};




//+
Curve Loop(45) = {49, -48, -52, 44};
//+
Surface(25) = {45};
//+
Curve Loop(47) = {52, -47, -51, 43};
//+
Surface(26) = {47};
//+
Curve Loop(49) = {42, 51, -46, -50};
//+
Surface(27) = {49};
//+
Curve Loop(51) = {50, -45, -49, 41};
//+
Surface(28) = {51};
//+
Curve Loop(53) = {56, -48, -54};
//+
Surface(29) = {53};
//+
Curve Loop(55) = {56, 45, 53};
//+
Surface(30) = {55};
//+
Curve Loop(57) = {55, -53, 46};
//+
Surface(31) = {57};
//+
Curve Loop(59) = {47, -54, -55};
//+
Surface(32) = {59};
//+




//+
Curve Loop(62) = {29, 25, -30, -21};
//+
Surface(34) = {62};
//+
Curve Loop(64) = {32, 28, -29, -24};
//+
Surface(35) = {64};
//+
Curve Loop(66) = {27, -32, -23, 31};
//+
Surface(36) = {66};
//+
Curve Loop(68) = {22, 31, -26, -30};
//+
Surface(37) = {68};
//+
Curve Loop(70) = {36, 25, 33};
//+
Surface(38) = {70};
//+
Curve Loop(72) = {28, -36, 34};
//+
Surface(39) = {72};
//+
Curve Loop(74) = {27, -34, -35};
//+
Surface(40) = {74};
//+
Curve Loop(76) = {33, -35, -26};
//+
Surface(41) = {76};
//+
Curve Loop(78) = {24, 21, 22, 23};
//+
Plane Surface(42) = {78};







//+
Curve Loop(89) = {105, 106, 107, 108};
//+
Plane Surface(49) = {89};
//+
Curve Loop(90) = {110, -105, -109, 101};
//+
Plane Surface(50) = {90};
//+
Curve Loop(91) = {111, -106, -110, 102};
//+
Plane Surface(51) = {91};
//+
Curve Loop(93) = {111, 107, -112, -103};
//+
Plane Surface(53) = {93};
//+
Curve Loop(94) = {109, -108, -112, 104};
//+
Plane Surface(54) = {94};



//+
Curve Loop(79) = {89, -88, -92, 84};
//+
Plane Surface(43) = {79};
//+
Curve Loop(80) = {87, 88, 85, 86};
//+
Plane Surface(44) = {80};
//+
Curve Loop(81) = {92, -87, -91, 83};
//+
Plane Surface(45) = {81};
//+
Curve Loop(82) = {89, 85, -90, -81};
//+
Plane Surface(46) = {82};
//+
Curve Loop(83) = {86, -91, -82, 90};
//+
Plane Surface(47) = {83};
//+
Curve Loop(84) = {81, 82, 83, 84};
//+
Curve Loop(85) = {64, 61, 62, 63};
//+
Curve Loop(86) = {2, 3, 4, 1};
//+
Curve Loop(87) = {42, 43, 44, 41};
//+
Curve Loop(88) = {24, 21, 22, 23};



//+
Curve Loop(95) = {102, 103, 104, 101};
//+
Curve Loop(96) = {44, 41, 42, 43};
//+
Curve Loop(97) = {24, 21, 22, 23};
//+
Curve Loop(98) = {102, 103, 104, 101};
//+
Curve Loop(99) = {44, 41, 42, 43};
//+
Curve Loop(100) = {24, 21, 22, 23};
//+
Curve Loop(101) = {63, 64, 61, 62};
//+
Curve Loop(102) = {3, 4, 1, 2};
//+
Plane Surface(55) = {98, 99, 100, 101, 102};

//+
Curve Loop(103) = {82, 83, 84, 81};
//+
Curve Loop(104) = {103, 104, 101, 102};
//+
Plane Surface(56) = {103, 104};




//+
Surface Loop(2) = {34, 37, 36, 40, 39, 35, 38, 41, 42};
//+
Volume(2) = {2};
//+
Surface Loop(3) = {17, 18, 20, 23, 22, 16, 19, 21, 24};
//+
Volume(3) = {3};

//+
Surface Loop(4) = {47, 44, 45, 43, 46, 56, 53, 51, 49, 50, 54};
//+
Volume(4) = {4};
//+
Surface Loop(5) = {53, 51, 49, 50, 54, 55, 25, 28, 27, 26, 32, 31, 30, 29, 36, 37, 41, 38, 39, 40, 35, 34, 16, 17, 18, 20, 23, 22, 21, 19, 9, 8, 7, 10, 14, 11, 12, 13};
//+
Volume(5) = {5};

//+
Physical Volume("tissu", 201) = {4, 5};
//+
Physical Volume("petites_electrodes", 202) = {2, 3};
//+
Physical Surface("electrodes_i_plus", 203) = {26, 25, 28, 27, 29, 32, 31, 30};
//+
Physical Surface("electrodes_i_moins", 204) = {9, 10, 7, 8, 12, 11, 13, 14};
//+
Physical Surface("electrodes_v_plus", 205) = {34, 36, 37, 40, 39, 35, 38, 41, 42};
//+
Physical Surface("electrodes_v_moins", 206) = {17, 19, 16, 18, 24, 21, 22, 23, 20};





