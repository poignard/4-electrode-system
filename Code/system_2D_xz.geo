//+
SetFactory("OpenCASCADE");
//+
Mesh.MshFileVersion = 2.2;


lc = 0.0001*1000;

r = 0.000125*1000;
h = 0.003*1000;
l = 0.0028*1000;
R = 0.000225*1000;
H = 0.0063*1000;
L = 0.0046*1000;

A = 0.008*1000;
B = 0.010*1000;

Abis = 0.005*1000;
Bbis = 0.007*1000;

//+
Rectangle(1) = {-A, -B, 0, 2*A, B, 0};
//+
Rectangle(2) = {-Abis, -Bbis, 0, 2*Abis, Bbis, 0};
//+
BooleanDifference{ Surface{1}; Delete; }{ Surface{2}; }

//+
Rectangle(3) = {-L/2-R, -H, 0, 2*R, H, 0};
//+
Disk(4) = {-L/2, -H, 0, R, R};
//+
BooleanUnion{ Surface{3}; Delete; }{ Surface{4}; Delete; }
//+
Rectangle(6) = {+L/2-R, -H, 0, 2*R, H, 0};
//+
Disk(7) = {+L/2, -H, 0, R, R};
//+
BooleanUnion{ Surface{6}; Delete; }{ Surface{7}; Delete; }
//+
BooleanDifference{ Surface{2}; Delete; }{ Surface{4}; Surface{3}; Surface{5}; Surface{7}; Surface{6}; Surface{8}; Delete; }


//+
Rectangle(3) = {-l/2-r, -h, 0, 2*r, h, 0};
//+
Disk(4) = {-l/2, -h, 0, r, r};
//+
BooleanUnion{ Surface{3}; Delete; }{ Surface{4}; Delete; }

//+
Rectangle(6) = {+l/2-r, -h, 0, 2*r, h, 0};
//+
Disk(7) = {+l/2, -h, 0, r, r};
//+
BooleanUnion{ Surface{6}; Delete; }{ Surface{7}; Delete; }
//+
BooleanDifference{ Surface{2}; Delete; }{ Surface{4}; Surface{3}; Surface{5}; Surface{7}; Surface{6}; Surface{8}; Delete; }


//+
Physical Surface("box") = {1, 2};



//+
Characteristic Length {30, 31, 22, 21, 26, 25, 34, 35} = lc;
//+
Characteristic Length {29, 32, 23, 24, 27, 28, 33, 36} = 2*lc;
//+
Characteristic Length {8, 5, 6, 7} = 4*lc;
