//+
SetFactory("OpenCASCADE");
//+
Mesh.MshFileVersion = 2.2;


lc = 0.0001*1000;

r = 0.000125*1000;
l = 0.0028*1000;
R = 0.000225*1000;
L = 0.0046*1000;

A = 0.008*1000;
a = 0.004*1000;
Abis = 0.005*1000;
abis = 0.001*1000;

//+
Rectangle(1) = {-A, -a, 0, 2*A, 2*a, 0};
//+
Rectangle(2) = {-Abis, -abis, 0, 2*Abis, 2*abis, 0};
//+
BooleanDifference{ Surface{1}; Delete; }{ Surface{2}; }



//+
Disk(3) = {L/2, 0, 0, R, R};
//+
Disk(4) = {-L/2, 0, 0, R, R};
//+
BooleanDifference{ Surface{2}; Delete; }{ Surface{4}; Surface{3}; Delete; }

//+
Disk(3) = {l/2, 0, 0, r, r};
//+
Disk(4) = {-l/2, 0, 0, r, r};
//+
BooleanDifference{ Surface{2}; Delete; }{ Surface{4}; Surface{3};  Delete;}


//+
Physical Surface("box") = {2, 1};



//+
Characteristic Length {17, 16, 15, 18} = lc;
//+
Characteristic Length {8, 5, 7, 6} = 4*lc;
